DECLARE
  query_id number(10);
BEGIN

-- CONSULTA
  insert into rgen_query ( id, description, editable, sql ) values ( seq_rgen_query.nextval, 'Historico Saidas', 0, q'[  select estq_300_estq_310.nivel_estrutura || '.' || estq_300_estq_310.grupo_estrutura || '.' || estq_300_estq_310.subgrupo_estrutura || 
  '.' || estq_300_estq_310.item_estrutura Produto, 
  sum(estq_040.qtde_estoque_atu) Estoque,
  sum(tmrp_040.qtde_areceber)    AReceber, 
  sum(estq_300_estq_310.quantidade) Movimento,
  sum(estq_040.qtde_estoque_atu + tmrp_040.qtde_areceber - estq_300_estq_310.quantidade) Saldo
  from estq_300_estq_310, estq_040, tmrp_040
  where estq_040.cditem_nivel99 = estq_300_estq_310.nivel_estrutura
  and estq_040.cditem_grupo = estq_300_estq_310.grupo_estrutura
  and estq_040.cditem_subgrupo = estq_300_estq_310.subgrupo_estrutura
  and estq_040.cditem_item = estq_300_estq_310.item_estrutura
  and estq_300_estq_310.nivel_estrutura = tmrp_040.co_reser_nivel99
  and estq_300_estq_310.grupo_estrutura = tmrp_040.co_reser_grupo
  and estq_300_estq_310.subgrupo_estrutura = tmrp_040.co_reser_subgrupo
  and estq_300_estq_310.item_estrutura = tmrp_040.co_reser_item
  and entrada_saida = 'S'
  and nivel_estrutura = '1'
  and tmrp_040.periodo_producao between {periodo_prod_ini} and {periodo_prod_fim}
  and data_movimento between {data_movimento_ini} and {data_movimento_fim}
  group by estq_300_estq_310.data_movimento, tmrp_040.periodo_producao, estq_300_estq_310.nivel_estrutura, 
  estq_300_estq_310.grupo_estrutura, estq_300_estq_310.subgrupo_estrutura, estq_300_estq_310.item_estrutura]' ) RETURNING id INTO query_id;


-- CAMPOS DA CONSULTA
  insert into rgen_query_field ( id, name, type, query_id ) values ( seq_rgen_query_field.nextval, 'PRODUTO', 'text', query_id );
  insert into rgen_query_field ( id, name, type, query_id ) values ( seq_rgen_query_field.nextval, 'ESTOQUE', 'integer', query_id );
  insert into rgen_query_field ( id, name, type, query_id ) values ( seq_rgen_query_field.nextval, 'ARECEBER', 'integer', query_id );
  insert into rgen_query_field ( id, name, type, query_id ) values ( seq_rgen_query_field.nextval, 'MOVIMENTO', 'integer', query_id );
  insert into rgen_query_field ( id, name, type, query_id ) values ( seq_rgen_query_field.nextval, 'SALDO', 'integer', query_id );


-- PARAMETROS DA CONSULTA
  insert into rgen_query_params ( id, name, query_id, default_value, fyi_message ) values ( seq_rgen_query_params.nextval, '{periodo_prod_ini}', query_id, '', '' );
  insert into rgen_query_params ( id, name, query_id, default_value, fyi_message ) values ( seq_rgen_query_params.nextval, '{periodo_prod_fim}', query_id, '', '' );
  insert into rgen_query_params ( id, name, query_id, default_value, fyi_message ) values ( seq_rgen_query_params.nextval, '{data_movimento_ini}', query_id, '', '' );
  insert into rgen_query_params ( id, name, query_id, default_value, fyi_message ) values ( seq_rgen_query_params.nextval, '{data_movimento_fim}', query_id, '', '' );


COMMIT;

EXCEPTION
  WHEN DUP_VAL_ON_INDEX THEN
    NULL;
END;