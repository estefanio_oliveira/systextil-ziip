Como criar um plugin para o Syst�xtil
=====================================

 1 Criar o projeto
------------------
 - Crie a pasta onde estar� o projeto no seu workspace. De prefer�ncia, o nome da pasta segue o padr�o `systextil-meuplugin` (que � algo como "systextil-cliente" ou "systextil-funcionalidade").
 - Descompacte ali o conte�do do arquivo `systextil-plugin-template.zip` que est� dispon�vel na pasta padr�o de bibliotecas (que � `\\desenv01\lib`.
 - Siga as instru��es do arquivo `README.md` (que � *este arquivo*).
 - Conv�m substituir o nome gen�rico do projeto (que � "systextil-plugin") pelo nome espec�fico nos arquivos de configura��o, que s�o `.project` para o Eclipse e `build.xml` para o NetBeans.
 - Importe o projeto para o reposit�rio de c�digos-fonte de sua prefer�ncia.

A partir da� o projeto pode ser editado usando a IDE de sua prefer�ncia (j� configurado para Eclipse e NetBeans) e integrado ao controle de vers�es.

2 Editar o projeto
------------------
O projeto j� vem configurado em pastas conforme a finalidade: pastas para c�digos-fonte Java, para arquivos de recursos, para testes, para formul�rios do NXJ, para scripts SQL e para arquivos de texto. Basta editar o projeto como qualquer outro projeto Java do Syst�xtil, usando o arquivo `build.xml` para fazer as constru��es.

3 Integrar o projeto
--------------------
O plugin n�o servir� para nada se n�o estiver integrado ao Syst�xtil. Dependendo de como ele foi concebido, existe uma maneira de fazer essa integra��o.

Se o plugin contiver **uma parte de um processo** (p. ex. um plugin de cliente ou com uma l�gica de terceiros), � necess�rio alterar o projeto `systextil-plugins-api` para ali definir as classes da API que o plugin vai implementar. Fazer assim:

 - Criar a package do plugin (p. ex. `systextil.plugin.meuplugin`).
 - Definir a interface do provedor (o nome pode ser `Provedor` mesmo).
 - Definir uma ou mais interfaces ou classes abstratas que o provedor vai fornecer.
 - Se for conveniente, definir um ou mais DTOs para transporte de dados (obviamente).

Se o plugin contiver **um processo inteiro** (isto �, ele mesmo cont�m os formul�rios ou classes "batch" que executam o processo), ent�o n�o � preciso definir uma API, mas � preciso incluir o plugin no classloader, conforme descrito a seguir.

 - Se o arquivo do plugin for carregado estaticamente (isto �, no mesmo classloader da aplica��o), � preciso incluir o caminho para o artefato na se��o `Class-Path` dos arquivos `manifest.mf` e `manifest-nxj.mf`, conforme o caso.
 - Se o arquivo do plugin usar um classloader din�mico (o que permite trocar o arquivo sem reiniciar a aplica��o -- dispon�vel apenas com o uso de API), n�o � preciso alterar arquivos `.mf`, mas esse classloader deve estar implementado na pr�pria API.

(Ali j� est�o presentes alguns plugins que podem servir de exemplo.)

Feito isto, se o plugin implementa a API, � preciso atualizar o arquivo `systextil-plugins-api.jar` na pasta de bibliotecas do seu workspace, para que seja implementado pelo projeto do plugin e usado pelo(s) projetos(s) do Syst�xtil que usar�o o plugin.

> Observe que o artefato gerado pelo plugin (arquivo `.jar`) nunca fica no classpath do *c�digo-fonte* dos projetos, pois nunca � referenciado diretamente. Todas as chamadas s�o feitas atrav�s das classes da API.

.

> Written with [StackEdit](https://stackedit.io/).